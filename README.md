# MP2-FA19-PART 1

This project will building a dataset consisting of the homepages of faculty memebers from UC Merced Computer Science and Engineering department.
To achieve this, program will identify the faculty directory listing page where all the faculty members are listed and get the urls for the homepages of all faculty members. Program will scrape all the text information, such as the faculty bio, from the faculty homepage.

1. This program will scrape  UC Merced Computer Science & Engineering faculty  listing page per [sign up sheet](https://docs.google.com/spreadsheets/d/198HqeztqhCHbCbcLeuOmoynnA3Z68cVxixU5vvMuUaM/edit?usp=sharing). Refer row with username:sunilk2 
   
2. Program scrapes the faculty listing page and all the faculty homepages using Python. Finally, it will  generate two output .txt files called **bios.txt** and **bio_urls.txt**. 'bio_urls.txt' should contain the urls of all the faculty homepages and have one url per line. 'bios.txt' should contain the text scraped from all the urls in 'bio_urls.txt', and again have one document per line. The two files must have the same order, that is, the first line in 'bios.txt' should correspond to the text extracted from the url in the first line of 'bio_urls.txt'. 

#### EWS Machine: username: sunilk2


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

1. Python3 is essential to run the project. All EWS machines have python3 pre-installed, else install python3. Lot of material on the web on how to install python3. EWS machine already has pre-installed python3, this step is not necessary if code will be executed on UIUC EWS machine.

2. Create Python3 virtual environment: Name of the virtual environment is "python3-venv". Once created using following command, it will create python3-venv directory. This command was executed from my home "/home/sunilk2"
    
    Note: Following is already created on EWS machine. So if you plan to login into sunilk2 account on EWS machine to run app then following step is not required.
    ```
    python3 -m venv python3-venv
    ```

3. Activate Virtual environment.

    ```
    source python3-venv/bin/activate
    ```
    To ensure Virtual environment is activated, run following command
    ```pip -V``` It should display pip version from "python3-venv" directory.

4. Upgrade to latest PIP. Following step not required on EWS machine.

    ```
    pip install pip --upgrade
    ```

5. You can check PIP version using command. Following step not required on EWS machine.

    ```
    pip --version
    ```

### Installing

A step by step series of examples that tell you how to get a development env running

Download project from [GIT](https://lab.textdata.org/sunilk2/MP2-FA19_part1.git), once downloaded it will have the following structure
```git clone https://lab.textdata.org/sunilk2/MP2-FA19_part1.git```

```MP2-FA19_part1
├── README.md
├── bio_urls.txt
├── bios.txt
└── scraper_code
├──────scraper.py
├──────requirements.txt
```

Change directory to $MP2-FA19_part1/scraper_code$

```
$MP2-FA19_part1/scraper_code$
```

Download necessary packages to run project. If running on EWS machine using sunilk2 account then not required to execute the following command.

```
pip install -U -r requirements.txt
```


## Run to scrape faculty URLs and bios

Change directory to 

```
$MP2-FA19_part1/scraper_code$
```
Run 
```
python scraper.py
```

You will see two files in $MP2-FA19_part1

```
bio_urls.txt and bios.txt
```

1. 'bio_urls.txt'  contains the urls of all the faculty homepages and have one url per line. 
2. 'bios.txt'  contains the text scraped from all the urls in 'bio_urls.txt', and again have one document per line.
3. The two files must have the same order, that is, the first line in 'bios.txt' should correspond to the text extracted from the url in the first line of 'bio_urls.txt'. 



## Built With

* [beautifulsoup4](https://pypi.org/project/beautifulsoup4/) - Beautiful Soup is a library that makes it easy to scrape information from web pages.
* [requests](https://pypi.org/project/requests/2.7.0/) - Requests is an Apache2 Licensed HTTP library, written in Python, for human beings.

## Authors

* **Sunil Kulkarni** - sunilk2@illinois.edu

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

