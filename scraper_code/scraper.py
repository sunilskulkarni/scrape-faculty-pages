from bs4 import BeautifulSoup
import requests
import csv
from bs4.element import Comment
import re
import os.path




def tag_visible(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]', 'noscript', 'header','html','input']:
        return False
    elif re.match(r'[\s\r\n]+', str(element)):
        return False
    if isinstance(element, Comment):
        return False

    return True


def text_from_html(body):
    soup = BeautifulSoup(body.text, 'html.parser')
    texts = soup.findAll(text=True)
    visible_texts = filter(tag_visible, texts)
    return u" ".join(t.strip() for t in visible_texts)



def visible_text_on_webpage(url):
    headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36'}

    page = requests.get(url, headers=headers, timeout=5)

    lines = text_from_html(page)
    #print(lines.rstrip())
    return lines.rstrip()

def scrape_uc_merced_faculty_urls_detailed_profile():
    # Writing data to bios.txt
    path_bio_urls = os.path.join(os.path.dirname(os.path.realpath(__file__)),'..', 'bio_urls.txt')
    path_bios = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'bios.txt')

    with open(path_bio_urls, 'w') as file_bio_urls, open(path_bios, 'w') as file_bio:
        f_bio_urls = csv.writer(file_bio_urls)
        f_bios = csv.writer(file_bio)

        uc_merced_url = 'https://engineering.ucmerced.edu'

        # There are 2 pages that needs to be paginated to extract names and URLs.
        profile_pages = []
        url = 'https://engineering.ucmerced.edu/soe-bylaw-units/computer-science-engineering-cse'
        pages = []
        for i in range(0, 2):
            if(i > 0):
                url = url + '?page=' + str(i)
            pages.append(url)


        for item in pages:


            page = requests.get(item)
            soup = BeautifulSoup(page.text, 'html.parser')

            last_links = soup.find(class_='span2')
            last_links.decompose()

            last_links = soup.find(class_='span7 researchInt')
            last_links.decompose()

            faculty_list = soup.findAll(class_='span12 peopleContain')
            for faculty in faculty_list:
                faculty_homepage = faculty.find(class_='span3').find('h3').find('a')
                faculty_link = uc_merced_url + faculty_homepage.get('href')

                detailed_profile, detailed_link = detailed_profile_page_link(faculty_link)
                #print(detailed_profile)

                if not detailed_link:
                    f_bio_urls.writerow([faculty_link])
                    f_bios.writerow([detailed_profile])

                else:
                    f_bio_urls.writerow([detailed_link])
                    detailed_profile = detailed_profile + text_from_webpage(detailed_link)
                    f_bios.writerow([detailed_profile])



def detailed_profile_page_link(url):

    headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36'}

    page = requests.get(url,headers=headers,timeout=5)
    soup = BeautifulSoup(page.text, 'html.parser')
    detailed_profile = soup.find(class_='node node-people clearfix')
    detailed_profile1 = detailed_profile.get_text()
    detailed_profile = detailed_profile1.replace("\n", "")

    #Get website link in detailed profile page
    detailed_profile_link = soup.find('div',
                                      class_='field field-name-field-people-faculty-lab-site field-type-link-field field-label-above')
    if detailed_profile_link is not None:
        link = detailed_profile_link.find('a')
        return detailed_profile, link.get('href')
    else:
        return detailed_profile, ''


def text_from_webpage(url):

    headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36'}

    page = requests.get(url,headers=headers, timeout=5)
    soup = BeautifulSoup(page.text, 'html.parser')
    # kill all script and style elements
    for script in soup(['style', 'script']):
        script.extract()  # rip it out

    # get text
    text = soup.get_text()

    # break into lines and remove leading and trailing space on each
    lines = (line.strip() for line in text.splitlines())
    # break multi-headlines into a line each
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
    # drop blank lines
    text = ' '.join(chunk for chunk in chunks if chunk)

    #print(text.rstrip())

    return text.rstrip()


if __name__ == '__main__':

    print('Scraping UC Merced Computer Science and Engineering faculty urls and respective Bios')
    scrape_uc_merced_faculty_urls_detailed_profile()
    print('bio_urls.txt and bios.txt created one level up this current directory')

    #visible_text_on_webpage('http://faculty.ucmerced.edu/dong-li/')
    #detailed_profile_page_link('https://engineering.ucmerced.edu/content/ahmed-arif')

    # detailed_profile, detailed_link = detailed_profile_page_link('https://engineering.ucmerced.edu/content/stefano-carpin')
    # print(detailed_profile)
    # print(detailed_link)

    # page = text_from_webpage('http://faculty.ucmerced.edu/frusu')
    # print(page)
    # print('-------')
    # visible_text_on_webpage('https://engineering.ucmerced.edu/content/stefano-carpin')
